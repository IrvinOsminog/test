#-------------------------------------------------
#
# Project created by QtCreator 2016-09-04T09:14:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MineSweeper
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    field.cpp \
    cell.cpp

HEADERS  += mainwindow.h \
    field.h \
    cell.h

FORMS    += mainwindow.ui
RESOURCES += images.qrc
