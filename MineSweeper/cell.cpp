#include "cell.h"

//Cell class. Contains single cell definition, click events, opening and marking methods.

//constructor
Cell::Cell(QWidget* parent, int quality)
{
    this->setParent(parent);
    this->setPixmap(QPixmap(":/images/not_flat_button.png"));
    this->adjustSize();
    this->resize(this->size().width() * 1.1, this->size().height() * 1.1);
    this->Quality = quality;
    this->isOpened = false;
    this->isMarked = false;
    this->setMouseTracking(true);
    this->installEventFilter(this);

    connect(this, SIGNAL(pressed()), this, SLOT(slotPressed()));

}

//destructor
Cell::~Cell()
{
    disconnect(this, SIGNAL(pressed()), this, SLOT(slotPressed()));
    this->setParent(NULL);
    this->setPixmap(QPixmap(0, 0));
}

//right button click - mark cell
bool Cell::Mark()
{
    if (!Cell::isMarked)
    {
        this->setPixmap(QPixmap(":/images/flag_no_flat_button.png"));
        Cell::isMarked = true;
    }
    else
    {
       this->setPixmap(QPixmap(":/images/not_flat_button.png"));
       Cell::isMarked = false;
    }

    return isMarked;
}

//left button click - open cell
void Cell::Open()
{   
    if (this->isOpened)
        return;

    QPixmap _cellIcon;
    this->isOpened = true;

    if (this->isMarked == true && this->Quality == -1)
    {
        _cellIcon = QPixmap(":/images/mine_disarmed_flat_button.png");
    }
    else
    {
        switch (this->Quality)
        {
        case -1:
            _cellIcon = QPixmap(":/images/mine_flat_button.png");
            break;
        case 0:
            _cellIcon = QPixmap(":/images/flat_button.png");
            break;
        case 1:
            _cellIcon = QPixmap(":/images/one_flat_button.png");
            break;
        case 2:
            _cellIcon = QPixmap(":/images/two_flat_button.png");
            break;
        case 3:
            _cellIcon = QPixmap(":/images/three_flat_button.png");
            break;
        case 4:
            _cellIcon = QPixmap(":/images/four_flat_button.png");
            break;
        case 5:
            _cellIcon = QPixmap(":/images/five_flat_button.png");
            break;
        case 6:
            _cellIcon = QPixmap(":/images/six_flat_button.png");
            break;
        case 7:
            _cellIcon = QPixmap(":/images/seven_flat_button.png");
            break;
        case 8:
            _cellIcon = QPixmap(":/images/eight_flat_button.png");
            break;
        default:
            _cellIcon = QPixmap(":/images/not_flat_button.png");
            break;
        }
    }

    this->setPixmap(_cellIcon);
    this->adjustSize();
}

//set event filter to catch mouse click on cell
bool Cell::eventFilter(QWidget* obj, QMouseEvent* ev)
{
    std::cout << "Event!";
    if (ev->button() == Qt::LeftButton)
    {
        std::cout << "Got mouse event!" << std::endl;
        emit pressed();
    }
    return QWidget::event(ev);
}

void Cell::slotPressed()
{
    std::cout << "Slot emited!" << std::endl;
}
