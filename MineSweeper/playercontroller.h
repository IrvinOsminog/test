#ifndef PLAYERCONTROLLER_H
#define PLAYERCONTROLLER_H

#include <QPoint>
#include <QWidget>
#include <QMouseEvent>
#include "field.h"

using namespace std;

class PlayerController
{
//    Q_OBJECT
signals:
    void mousePressed( const QPoint& );
public:
    PlayerController(QWidget* widget, Field* field);
    void OpenCell(QPoint point);
    Field* field;
private:
    QWidget* mainWidget;
    void eventFilter(QObject *obj, QEvent *event);
};

#endif // PLAYERCONTROLLER_H
