#include "playercontroller.h"
#include "mainwindow.h"

PlayerController::PlayerController(QWidget* widget, Field* field)
{
    this->mainWidget = widget;
    this->field = field;
}

void PlayerController::OpenCell(QPoint pos)
{
    QSize _s = field->cells[0]->cellObject->size();
    int _cellIndex = pos.y()/_s.height() + field->height * (pos.x()/_s.width());

    if (field->cells[_cellIndex] != 0)
    {
        field->cells[_cellIndex]->Open();
    }
}
