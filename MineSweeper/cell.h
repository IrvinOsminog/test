#ifndef CELL_H
#define CELL_H

#include <QImage>
#include <QLabel>
#include <QWidget>
#include <QEvent>
#include <QLayout>
#include <QMouseEvent>
#include <iostream>
#include <QObject>
#include <QtWidgets>

class Cell : public QLabel
{
    Q_OBJECT

public:
    explicit Cell(QWidget* parent = 0, int quality = 0);
    ~Cell();

    int Quality = 0;

    bool isOpened = false;
    bool isMarked = false;

    void Open ();
    bool Mark ();

signals:
    void pressed();

public slots:
    void slotPressed();

protected:
    bool eventFilter(QWidget* obj, QMouseEvent*  ev);
};

#endif //CELL_H
