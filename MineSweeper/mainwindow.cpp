#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "field.h"

// MainWindow class. Contains GUI realisation and click events.

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qApp->installEventFilter(this);

    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTimer()));

    ResetGame();
    connect(ui->actionBeginner_2, SIGNAL(triggered()), this, SLOT(RestartWithBeginner()));
    connect(ui->actionIntermediate_2, SIGNAL(triggered()), this, SLOT(RestartWithIntermediate()));
    connect(ui->actionExpert_2, SIGNAL(triggered()), this, SLOT(RestartWithExpert()));
}

MainWindow::~MainWindow()
{
    field = NULL;
    timer = NULL;
    delete ui;
}

void MainWindow::DrawField(Field* field)
{
    int _xGap = (field->cells[0]->size().width());
    int _yGap = (field->cells[0]->size().height());

    for (int i = 0; i< field->width; i++)
    {
        for (int j = 0; j< field->height; j++)
        {
            Cell* _cell = field->cells[j + field->height * i];

            _cell->setGeometry(i*_xGap, j*_yGap, _cell->size().width(), _cell->size().height());
            qApp->installEventFilter(_cell);
        }
    }
    ui->Field->resize(field->width*_xGap*1.25, field->height*_yGap*1.5);
}

//catch mouse clicks
bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
  if (event->type() == QEvent::MouseButtonRelease)
  {
      if (obj == ui->Face)
          ResetGame();

      if (isGameFinished)
          return false;

      QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);

      QPoint point = QPoint(mouseEvent->pos().x() - ui->Field->x(), mouseEvent->pos().y() - ui->Field->y());

      if (point.x() < 0 || point.y() < 0)
          return false;
      if (point.x() > ui->Field->height() || point.y() > ui->Field->width())
          return false;

      if (!isGameStart)
      {
          timer->start(1000);
          isGameStart = true;
      }

      if (mouseEvent->button() == Qt::LeftButton)
      {
          int flag = this->field->OpenCell(point);
          if(flag == -1)
              LoseGame();
          else if(flag == 1)
              WinGame();
      }
      else if (mouseEvent->button() == Qt::RightButton)
      {
          if(this->field->MarkCell(point))
              ui->LCDBombs->display(ui->LCDBombs->value() - 1);
          else
              ui->LCDBombs->display(ui->LCDBombs->value() + 1);
      }

      return true;
  }
  return false;
}

void MainWindow::updateTimer()
{
    currentTime++;
    ui->LCDTimer->display(currentTime);
}

void MainWindow::WinGame()
{
    cout << "You win!" << endl;

    ui->Face->setPixmap(QPixmap (":/images/awesome_face.png"));

    this->isGameFinished = true;
    this->field->OpenAllBombs(true);
    this->timer->stop();
}

void MainWindow::LoseGame()
{
    cout << "You lose!" << endl;

    ui->Face->setPixmap(QPixmap(":/images/sad_face.png"));

    this->isGameFinished = true;
    this->timer->stop();
}

//game reinitializing
void MainWindow::ResetGame()
{
    ui->setupUi(this);
    timer->stop();
    currentTime = 0;

    this->field = new Field(this->fieldWidth, this->fieldHeight, this->fieldBombsCount, ui->Field);
    DrawField(this->field);

    ui->Face->setPixmap(QPixmap(":/images/normal_face.png"));
    ui->Face->adjustSize();

    this->isGameStart = false;
    this->isGameFinished = false;

     ui->LCDBombs->display(this->fieldBombsCount);

     this->resize(ui->Field->size());
}

// this functions aren't used
void MainWindow::RestartWithBeginner()
{
    this->fieldWidth = 8;
    this->fieldHeight = 8;
    this->fieldBombsCount = 10;
    this->ResetGame();
}

void MainWindow::RestartWithIntermediate()
{
    this->fieldWidth = 16;
    this->fieldHeight = 16;
    this->fieldBombsCount = 40;
    this->ResetGame();
}

void MainWindow::RestartWithExpert()
{
    this->fieldWidth = 30;
    this->fieldHeight = 16;
    this->fieldBombsCount = 99;
    this->ResetGame();
}

void MainWindow::on_actionExit_triggered()
{
    close();
    qApp->quit();
}
