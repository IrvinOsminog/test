#ifndef FIELD_H
#define FIELD_H

#include "cell.h"
#include <vector>
#include <iostream>

using namespace std;

class Field
{

public:
    int width;
    int height;
    int ClosedCells;

    vector<Cell*>   cells;
    vector<int>     bombsIndexes;
    vector<int>     flagsIndexes;

    Field(int width, int height, int bombQuantity, QWidget* parent);
    ~Field();

    int     SetCellNumber   (int x, int y);
    int     OpenCell        (QPoint pos);
    void    PrintField      ();
    void    OpenAllBombs    (bool isWin);
    bool    MarkCell        (QPoint pos);

private:
    QWidget* parent = 0;

    void    OpenNearestBlankCells   (int ancorCellX, int ancorCellY);
    void    CreateField             (int width, int height, int bombQuantity);
    bool    CheckIfWin              ();
};

#endif // FIELD_H
