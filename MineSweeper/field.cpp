#include "field.h"
#include <iostream>
#include <random>

//field class. Contains field costructor, and win/lose checks.

using namespace std;

//constructor
Field::Field(int width, int height, int bombQuantity, QWidget* parent)
{
    this->parent = parent;
    this->cells.reserve(width*height);
    this->CreateField(width, height, bombQuantity);
    this->ClosedCells= width*height;
}

//destructor
Field::~Field()
{
    flagsIndexes.clear();
    bombsIndexes.clear();
    cells.clear();
    parent = NULL;
}

void Field::CreateField(int width, int height, int bombQuantity)
{
    this->width = width;
    this->height = height;
    this->cells.clear();

    //check if ui->Field is blank
    QList<QWidget *> widgets = this->parent->findChildren<QWidget*>();
    foreach(QWidget * widget, widgets)
    {
        delete widget;
    }

    // instantiate field with blank buttons
    for (int i=0; i<Field::width; i++)
    {
        for (int j=0; j<Field::height; j++)
        {
            Field::cells.push_back(new Cell(Field::parent, 0));
        }
    }

    // set bombs
    int _bombs = 0;

    //instantiate random engine
    std::random_device rd;
    std::default_random_engine rng(rd());

    std::uniform_int_distribution<> w(0, Field::width-1);
    std::uniform_int_distribution<> h(0, Field::height-1);

    while (_bombs != bombQuantity)
    {
        int x = w(rng);
        int y = h(rng);

        if (Field::cells[y + Field::height * x]->Quality != -1)
        {
            Field::cells[y + Field::height * x]->Quality = -1;
            bombsIndexes.push_back(y + Field::height * x);
            _bombs++;
        }
        else
            continue;
    }

    // set numbers of bombs
    for (int i=0; i<Field::width; i++)
    {
        for (int j=0; j<Field::height; j++)
        {
          Field::cells[j + Field::height * i]->Quality = Field::SetCellNumber(i, j);
        }
    }
}

//finding bomb quantitty around current cell
int Field::SetCellNumber(int x, int y)
{
    int _bombCount = 0;

    if (Field::cells[y + Field::height * x]->Quality == -1)
        return -1;

    for (int i = -1; i<2; i++)
    {
        for (int j = -1; j<2; j++)
        {
            //if current cell - continue
            if (i==0 && j==0)
            {
                continue;
            }

            //if current cell is border cell - try to avoid negative indexes
            else if ((x+i)<0 || (x+i)>(Field::width-1) || (y+j)<0 || (y+j)>(Field::height-1))
                 continue;

            else if (Field::cells[(y+j) + Field::height * (x+i)]->Quality == -1)
                 _bombCount++;
        }
    }
    return _bombCount;
}

int Field::OpenCell(QPoint pos)
{
    QSize _s = Field::cells[0]->size();

    //calcuclate cell index
    int _cellIndex = static_cast<int>(pos.y()/_s.height()) + static_cast<int>(Field::height * (pos.x()/_s.width()));

    if (_cellIndex >= this->width*this->height)
        return 0;

    if (Field::cells[_cellIndex] != 0)
    {
        if (Field::cells[_cellIndex]->isOpened == false)
        {
            Field::cells[_cellIndex]->Open();
            this->ClosedCells--;


            if (Field::cells[_cellIndex]->Quality == 0)
            {
                Field::OpenNearestBlankCells(pos.x()/_s.width(), pos.y()/_s.height());
            }
            if (Field::cells[_cellIndex]->Quality == -1)
            {
                OpenAllBombs(false);
                return -1;
            }
        }
    }

    //when closed/marked cells quantity == bombs quantity - check if game is over
    if (this->ClosedCells == this->bombsIndexes.size())
    {
        if(this->CheckIfWin())
        {
            return 1;
        }
        else
            return 0;
    }
    return 0;
}

// opening nearest blank cells recursively
void Field::OpenNearestBlankCells(int ancorCellX, int ancorCellY)
{
    for (int i = -1; i<2; i++)
    {
        for (int j = -1; j<2; j++)
        {
            if (i==0 && j==0)
            {
                continue;
            }

            else if ((ancorCellX+i)<0 || (ancorCellX+i)>(Field::width-1) || (ancorCellY+j)<0 || (ancorCellY+j)>(Field::height-1))
                    continue;

            Cell* _currentCell = Field::cells[(ancorCellY+j) + Field::height * (ancorCellX+i)];

            if (_currentCell->isOpened)
                continue;

            if (_currentCell->Quality != -1)
            {
                    _currentCell->Open();

                    if(_currentCell->isOpened)
                        this->ClosedCells--;
            }

            if (_currentCell->Quality == 0)
            {
                Field::OpenNearestBlankCells(ancorCellX+i, ancorCellY+j);
            }
        }
    }
}

void Field::OpenAllBombs(bool isWin)
{
    if (isWin)
    {
        for (int i =0; i < this->bombsIndexes.size(); i++)
        {
            this->cells[this->bombsIndexes[i]]->isMarked = true;
            this->cells[this->bombsIndexes[i]]->Open();
        }
    }
    else
    {
        for (int i =0; i < this->bombsIndexes.size(); i++)
            this->cells[this->bombsIndexes[i]]->Open();
    }
}

bool Field::MarkCell(QPoint pos)
{
    bool isMarked = false;
    QSize _s = Field::cells[0]->size();
    int _cellIndex = pos.y()/_s.height() + Field::height * (pos.x()/_s.width());

    if (Field::cells[_cellIndex] != 0)
    {
        isMarked = Field::cells[_cellIndex]->Mark();
    }
    return isMarked;
}

bool Field::CheckIfWin()
{
    for (int i = 0; i < this->cells.size(); i++)
    {
        if (this->cells[i]->Quality != -1 && this->cells[i]->isOpened == false)
        {
            return false;
        }
    }
    return true;
}

// debuggind method
void Field::PrintField()
{
    for (int i = 0; i<Field::width; i++)
    {
        for (int j = 0; j<Field::height; j++)
        {
            if (Field::cells[j + Field::height * i]->Quality < 0)
                cout << " ";
            else
                cout << "  ";

            cout << Field::cells[j + Field::height * i]->Quality;

        }
        cout << endl;
    }
}
