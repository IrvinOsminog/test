#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "field.h"
#include <QVBoxLayout>
#include <QMouseEvent>
#include <QSignalMapper>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    Field*      field           = NULL;
    QTimer*     timer           = NULL;
    bool        isGameStart     = false;
    bool        isGameFinished  = false;

    int         currentTime     = 0;
    int         fieldWidth      = 8;
    int         fieldHeight     = 8;
    int         fieldBombsCount = 10;

    void UpdateTimer();

private:
    Ui::MainWindow *ui;

    void WinGame();
    void LoseGame();
    void ResetGame();

private slots:
    void     updateTimer    ();
    void     on_actionExit_triggered();
    void     DrawField      (Field* field);
    bool     eventFilter    (QObject *obj, QEvent *event);
    void     RestartWithBeginner();
    void     RestartWithIntermediate();
    void     RestartWithExpert();
};

#endif // MAINWINDOW_H
